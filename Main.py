import discord
from discord.ext import commands
from discord import Embed, Color
from termcolor import colored
import asyncio
import json
import os

# Local Imports (Mainly for Pyinstaller)
import modules.CheckUser
import modules.ElcheaChecks
import modules.PythonShell
import modules.Moderation
import modules.Music

# JSON being stupid
false = False
true = True

# Define Extensions for more Commands
startup_extensions = [ 'ElcheaChecks', 'PythonShell', 'Moderation', 'Music']

# Open / Create config.json
configPath = r'json' + os.sep +'config.json'

if os.path.exists(configPath):
    with open(configPath, 'r+') as f:
        config = json.load(f)
        f.close()
else:
    print(colored('Creating config.json...', 'green'))
    with open(configPath, 'a+') as f:
        config = {}
        config['accounts'] = {}
        json.dump(config, f, indent=4)

print(colored('Accounts:', 'blue'))
for accounts in config['accounts']:
    print('\t' + colored(accounts, 'yellow'))

name = input('Type account name to select account \nType "' + colored('new', 'cyan') + '" to create an account \n')

if name == 'new':
    name        = input('Account Name:\n')
    token       = input('Token:\n')
    prefix      = input('Command Prefix (Ex: !, ?, $, #)\n')
    description = input('Bot Description\n')

    # Remove ' from token
    token.replace('"', '')

    # Input Loop for if the account is a bot
    while True:
        isbot = input('Is a Bot account? t/f\n')

        if isbot == 'true' or isbot == 't':
            isbot = True
            break
        elif isbot == 'false' or isbot == 'f':
            isbot = False
            break
        else:
            print(colored('Invalid input, please try again\n', 'red'))
    
    # Input Loop for if the account is a self-bot (for debugging)
    while True:
        selfbot = input('Is a selfbot account? t/f\n')

        if selfbot == 'true' or selfbot == 't':
            selfbot = True
            break
        elif selfbot == 'false' or selfbot == 'f':
            selfbot = False
            break
        else:
            print(colored('Invalid input, please try again\n', 'red'))

    # Input Loop for if messages inducing commands should be auto-deleted
    while True:
        delcmd = input('Is a selfbot account? t/f\n')

        if selfbot == 'true' or delcmd == 't':
            selfbot = True
            break
        elif selfbot == 'false' or delcmd == 'f':
            selfbot = False
            break
        else:
            print(colored('Invalid input, please try again\n', 'red'))

    config['accounts'][name] = {}
    config['accounts'][name]['name']        = None
    config['accounts'][name]['id']          = None
    config['accounts'][name]['token']       = token
    config['accounts'][name]['prefix']      = prefix
    config['accounts'][name]['description'] = description
    config['accounts'][name]['isbot']       = isbot
    config['accounts'][name]['selfbot']     = selfbot
    config['accounts'][name]['delcmd']      = delcmd

    with open(configPath, 'r+') as f:
        json.dump(config, f, indent=4)
        f.close()
        
else:
    token       = config['accounts'][name]['token']
    prefix      = config['accounts'][name]['prefix']
    description = config['accounts'][name]['description']
    isbot       = config['accounts'][name]['isbot']
    selfbot     = config['accounts'][name]['selfbot']
    delcmd      = config['accounts'][name]['delcmd'] 

# Create the Bot and set the prefix, description, and if it is a selfbot
bot = commands.Bot(command_prefix=prefix, description=description, self_bot=selfbot)
bot.add_check(modules.CheckUser.check_author, call_once=True)
bot.remove_command('help')

@bot.event
async def on_ready():
    print('Username: ' + colored(bot.user.name, 'magenta'))
    print('ID: ' + colored(str(bot.user.id), 'magenta'))

    # Updates the Name / ID in the config.json
    config['accounts'][name]['name'] = bot.user.name
    config['accounts'][name]['id'] = bot.user.id
    with open(configPath, 'r+') as f:
        json.dump(config, f, indent=4)
        f.close()

# Load Extenstions
if __name__ == '__main__':
    for extension in startup_extensions:
        bot.load_extension('modules.' + extension)

# Delete message after invoking a cmd
@bot.after_invoke
async def clean(ctx):
    if delcmd:
        await ctx.message.delete()
        return
    else:
        return

# Custom Help Page
@bot.command(pass_context=True)
async def help(ctx, *args):
    'Help Page'
    title = name + ' Help Page'
    embed = Embed(title=title, description=description, color=1150895)
    cogs = bot.cogs
    cmds = bot.all_commands
    
    if args:
        for arg in args:
            for cmd in cmds:
                if arg == cmd:
                    embed = Embed(title=cmd, description=cmds[cmd].help, color=1150895)
                    embed.add_field(name='Usage:', value=cmds[cmd].signature)
                    if cmds[cmd].description != '':
                        embed.add_field(name='Description:', value=cmds[cmd].description)
                    await ctx.send(embed=embed)
          
    else:
        for cog in bot.cogs:
            cmdlist = ''
            for cmd in cmds:
                if cmds[cmd].hidden != True and cmds[cmd].cog_name == cog:
                    cmdlist += cmd + '\t-\t' + cmds[cmd].help + '\n'
            embed.add_field(name=cog + ':', value=cmdlist)
        embed.set_thumbnail(url=bot.user.avatar_url)
        await ctx.send(embed=embed)
    
# Init Selfbot
bot.run(token, bot = isbot)