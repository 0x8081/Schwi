# Schwi Discord Bot
## Made for the No Game No Life's Elchea Team

### Requirements:
* [Python 3.6.X](https://www.python.org/getit/)
* [discord.py 1.0.0a](https://github.com/Rapptz/discord.py/tree/rewrite)
* asyncio
* termcolor
* [ffmpeg](https://ffmpeg.zeranoe.com/builds/)
    * For Windows download and drop the binaries into Schwi's root directory
    * For Linux simply install **ffmepg** via your distro's packagemanager