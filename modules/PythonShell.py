import discord
from discord import Color
from discord.ext import commands
import collections
import inspect
import traceback
from contextlib import redirect_stdout
import io

class PythonShell():
    'Embeded Python Shell'
    def __init__(self, bot):
        self.bot = bot

    def cleanup_code(self, content):
        'Cleanup Code'
        newcode = content
        newcode.replace(str(self.bot.command_prefix) + 'run\n', '')
        return '\n'.join(newcode.split('\n')[2:-1])

        return content.strip('` \n')

    def get_syntax_error(self, err):
        '''Returns SyntaxError'''
        return '```py\n{0.text}{1:>{0.offset}}\n{2}: {0}```'.format(
            err,
            '^',
            type(err).__name__)
    
    @commands.command(pass_context=True, description="""
    Example:
    ```py
    import sys

    # Display host OS type
    print(sys.platform)
    ```
    """)
    async def run(self, ctx, *args):
        'Run Python Code'
        embed = discord.Embed(description='', color=Color.from_rgb(0, 0, 255))
        embed.set_author(
            name='Embeded Python Shell',
            icon_url='https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1024px-Python-logo-notext.svg.png')

        cleanedCode = self.cleanup_code(ctx.message.content)
        executor = exec
        if cleanedCode.count('\n') == 0:
            # single statement, potentially 'eval'
            try:
                code = compile(cleanedCode, 'code','eval')
            except SyntaxError:
                pass
            else:
                executor = eval

        if executor is exec:
                try:
                    code = compile(cleanedCode, 'code', 'exec')
                except SyntaxError as err:
                    return_msg = self.get_syntax_error(err)
                    #embed.color = ‭16776960‬
                    await ctx.send(return_msg, embed=embed)

        var = {
            'message': ctx.message,
            'code' : None
            }
        
        fmt = None
        stdout = io.StringIO()

        try:
            with redirect_stdout(stdout):
                result = executor(code, var)
                if inspect.isawaitable(result):
                    result = await result
                embed.color = Color.from_rgb(0, 255, 0)
        except Exception as err:
            value = stdout.getvalue()
            fmt = '```py\n{}{}\n```'.format(
                value,
                traceback.format_exc())
            fmt = '```py\n'+ ''.join(fmt.split('File "code",')[1])
            embed.color = Color.from_rgb(255, 0, 0)
        else:
            value = stdout.getvalue()

            if result is not None:
                fmt = '```py\n{}{}\n```'.format(
                    value,
                    result)

            elif value:
                fmt = '```py\n{}\n```'.format(value)

        try:
            if fmt is not None:
                if len(fmt) >= 800:
                    embed.color = Color.from_rgb(255, 255, 0)
                    await ctx.send('Output is too big', embed=embed)
                else:
                    embed.add_field(name='Output', value=fmt, inline=False)
                    await ctx.send(embed=embed)
                    embed.remove_field(-1)
            else:
                embed.color = Color.from_rgb(255, 255, 0)
                embed.add_field(name='No Code to Run', value='Assuming Success', inline=False)
                await ctx.send(embed=embed)
                embed.remove_field(-1)

        except discord.Forbidden:
            pass

        except discord.HTTPException as err:
            try:
                error_embed = discord.Embed(
                    color=Color.from_rgb(255, 0, 0),
                    description='**Error**: _{}_'.format(err))
                await ctx.send(embed=error_embed)
            except:
                pass
                    
def setup(bot):
    bot.add_cog(PythonShell(bot))