import discord
from discord.ext import commands
import io

#Category for Elchea score checking
class ElcheaChecks():
    'Commands for checking Elchea scores'

    #This is just for initing stuff, ignore this for now
    def __init__(self, bot):
        self.bot = bot
    
    #Defining the command for the bot
    @commands.command(pass_context=True, description="""
    Calculates an estimated score for Temple Run based off of:
     1. The Distance traveled
     2. The amount of Coins collected
     3. The Multiplier
     from a user's submitted screenshot""")
    async def TempleRun(self, ctx,  dist, coins, mult):
        'Estimate actual Temple Run score'
        
        scr = (int(dist) * int(mult)) + (int(coins) * 500)
        scr = str(scr)
        embed = discord.Embed(title='Estimated Score:', description=scr, color=1150895)
        await ctx.send(embed=embed)

#Command for adding all of this to the bot's cogs (basically addons)
def setup(bot):
    bot.add_cog(ElcheaChecks(bot))