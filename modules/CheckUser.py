from termcolor import colored

# Team Elchea Role ID
roleID = 464777779167363072

# NGNL Guild ID
guildID = 191455136013352960

def check_author(ctx):
    print('Checking User Permissions ' + colored(ctx.message.author.name, 'cyan') +
        '\nUser ID: ' + colored(str(ctx.message.author.id), 'cyan') +
        '\nCommand: ' + colored(ctx.message.content, 'yellow'))
    if ctx.guild != None:
        if ctx.guild.id == guildID:
            for i in ctx.message.author.roles:
                if i.id == roleID:
                    print(colored('Permission Granted', 'green'))
                    return True
            print(colored('Permission Denied', 'red'))
            return False
        else:
            print(colored('Permission Granted', 'green'))
            return True
    else:
        print(colored('Permission Granted', 'green'))
        return True
