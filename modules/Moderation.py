import discord
from discord.ext import commands
from discord import Embed, Color, Guild
import asyncio

class Moderation():
    'Commands for Server Moderation'

    def __init__(self, bot):
        self.bot = bot

    def get_user(self, mention):
        for user in self.bot.users:
            if user.mention == mention:
                return user
        return None

    @commands.command(pass_context=True)
    async def kick(self, ctx, user, *, reason=''):
        'Kicks a user (permissions required).'
        if ctx.message.author.guild_permissions.kick_members:
            guild = discord.Guild
            user = self.get_user(user)
            if user:
                await guild.kick(self=ctx.guild, user=user, reason=reason)
                return_msg = 'Kicked user `{}`'.format(user.mention)
                if reason:
                    return_msg += ' for reason `{}`'.format(reason)
                return_msg += '.'
                await ctx.send(return_msg)
            else:
                await ctx.send('Could not find user.')
        else:
            await ctx.send('Insufficent Permissions')

    @commands.command(pass_context=True)
    async def ban(self, ctx, user, *, reason=''):
        'Bans a user (permissions required).'
        if ctx.message.author.guild_permissions.kick_members:
            guild = discord.Guild
            user = self.get_user(user)
            if user:
                await guild.ban(self=ctx.guild, user=user, reason=reason)
                return_msg = 'Banned user `{}`'.format(user.mention)
                if reason:
                    return_msg += ' for reason `{}`'.format(reason)
                return_msg += '.'
                await ctx.send(return_msg)
            else:
                await ctx.send('Could not find user.')
        else:
            await ctx.send('Insufficent Permissions')

def setup(bot):
    bot.add_cog(Moderation(bot))